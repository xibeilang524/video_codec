# video_codec

#### 项目介绍
ffmpeg解码库，包括CPU版本和GPU版本，可直接读取rtsp视频流。

#### 安装教程

1. opencv3.2版本，opencv只用来测试显示用，不涉及解码转码等操作。
2. gpu解码的版本需要自己编译ffmpeg库，从https://github.com/FFmpeg/FFmpeg
上下载最新源码（该代码使用的版本为release里面的n4.1），用以下命令编译
./configure --enable-cuda --enable-cuvid --enable-nvenc --enable-nonfree --enable-libnpp 
--extra-cflags=-I/usr/local/cuda/include --extra-ldflags=-L/usr/local/cuda/lib64


#### ffmpeg更改部分
参考ffmpeg.patch，修改ffmpeg部分源码，解决rtsp流在网络丢包情况下会导致解码花屏的问题。

